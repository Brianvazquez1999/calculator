import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Calculator implements ActionListener {
    JFrame frame;
    JTextField textField;
    JButton[] numButtons = new JButton[10];
    JButton[] funcButtons = new JButton[9];
    JButton add, subtract, divide, multiply, clear, equal, decimal, delete, negButton;
    JPanel panel;
    Font myFont = new Font("Ink Free", Font.BOLD, 30);
    double num1 = 0, num2 = 0, result = 0;
    char operator;

    Calculator() {
        frame = new JFrame("Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(420, 550);
        frame.setLayout(null);
        textField = new JTextField();
        textField.setBounds(50, 25, 300, 50);
        textField.setFont(myFont);
        textField.setEditable(false);
        frame.add(textField);
        frame.setVisible(true);

        add = new JButton("+");
        subtract = new JButton("-");
        multiply = new JButton("X");
        divide = new JButton("/");
        clear = new JButton("CLR");
        equal = new JButton("=");
        decimal = new JButton(".");
        delete = new JButton("DELETE");
        negButton = new JButton("(-)");

        funcButtons[0] = add;
        funcButtons[1] = subtract;
        funcButtons[2] = multiply;
        funcButtons[3] = divide;
        funcButtons[4] = decimal;
        funcButtons[5] = delete;
        funcButtons[6] = clear;
        funcButtons[7] = equal;
        funcButtons[8] = negButton;

        for (int i =0; i < 9; i++) {
            funcButtons[i].addActionListener(this);
            funcButtons[i].setFont(myFont);
            funcButtons[i].setFocusable(false);

        }

        for (int i =0; i < 10; i++) {
            numButtons[i] = new JButton(String.valueOf(i));
            numButtons[i].addActionListener(this);
            numButtons[i].setFont(myFont);
            numButtons[i].setFocusable(false);
        }

        negButton.setBounds(50,430,100,50);
        delete.setBounds(150,430,100,50);
        clear.setBounds(250,430,100,50);
        panel = new JPanel();
        panel.setBounds(50,100,300,300);
        panel.setLayout(new GridLayout(4,4,10,10));

        panel.add(numButtons[1]);
        panel.add(numButtons[2]);
        panel.add(numButtons[3]);
        panel.add(add);
        panel.add(numButtons[4]);
        panel.add(numButtons[5]);
        panel.add(numButtons[6]);
        panel.add(subtract);
        panel.add(numButtons[7]);
        panel.add(numButtons[8]);
        panel.add(numButtons[9]);
        panel.add(multiply);
        panel.add(decimal);
        panel.add(numButtons[0]);
        panel.add(equal);
        panel.add(divide);

        frame.add(panel);
        frame.add(negButton);
        frame.add(delete);
        frame.add(clear);
    }

    public static void main(String[] args) {
        Calculator calc = new Calculator();
    }

    public void actionPerformed (ActionEvent e) {
        for(int i = 0; i < 10; i++) {
            if(e.getSource() == numButtons[i]) {
                textField.setText(textField.getText().concat(String.valueOf(i)));
            }
        }
        if(e.getSource() == decimal) {
            textField.setText(textField.getText().concat("."));
        }
        if (e.getSource() == add) {
            num1 = Double.parseDouble(textField.getText());
            operator = '+';
            textField.setText("");
        }
        if (e.getSource() == subtract) {
            num1 = Double.parseDouble(textField.getText());
            operator = '-';
            textField.setText("");
        }
        if (e.getSource() == divide) {
            num1 = Double.parseDouble(textField.getText());
            operator = '/';
            textField.setText("");
        }
        if (e.getSource() == multiply) {
            num1 = Double.parseDouble(textField.getText());
            operator = '*';
            textField.setText("");
        }

        if (e.getSource() == clear) {
            textField.setText("");
        }
        if (e.getSource() == negButton) {
            double value = Double.parseDouble(textField.getText());
            value*=-1;
            textField.setText(String.valueOf(value));
        }
        if (e.getSource() == delete) {
            String string = String.valueOf(textField.getText());
            textField.setText("");
            for(int i=0; i<string.length() -1; i++) {
                textField.setText(textField.getText()+string.charAt(i));
            }
        }
        if (e.getSource() == equal) {
            num2 = Double.parseDouble(textField.getText());
            switch(operator) {
                case '+':
                    result = num1+num2;
                    break;
                case '-':
                result = num1-num2;
                break;
                case '*':
                    result = num1*num2;
                    break;
                case '/':
                result = num1/num2;
                break;

            }
            textField.setText(String.valueOf(result));
            num1=result;
        }
    }

}
